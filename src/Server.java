
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.rmi.Remote;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author MF613110035
 */
public class Server
{

    public static void main(String args[])
    {
        try
        {

            Mahasiswa m = new Mahasiswa();
            m.setNama("Muhammad Fauzan");
            m.setNim("1301158599");

            RMIRegistry skeleton = (RMIRegistry) UnicastRemoteObject.exportObject(m, 9999);
            Registry reg = LocateRegistry.createRegistry(2100);

            reg.bind("mata lelaki", skeleton);
            System.out.println("Bind success");
        } catch (Exception e)
        {
            System.out.println("Error : " + e.getMessage());
        }
    }

}
