

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ZANDUT
 */
public class Mahasiswa implements RMIRegistry
{

    private String nama;
    private String nim;

    public void setNama(String nama)
    {
        this.nama = nama;
    }

    public void setNim(String nim)
    {
        this.nim = nim;
    }

    public String getNama()
    {
        return nama;
    }

    public String getNim()
    {
        return nim;
    }

    @Override
    public String getInfo() throws Exception
    {
        return "Ini Class Mahasiswa";
    }

    @Override
    public void tulis(String msg) throws Exception
    {
        System.out.println(msg);
    }

    @Override
    public String diplay() throws Exception
    {
        return "NIM : " + getNim() + "\nNama : " + getNama();
    }

}
